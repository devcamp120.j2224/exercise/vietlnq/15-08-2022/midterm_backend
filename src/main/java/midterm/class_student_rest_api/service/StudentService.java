package midterm.class_student_rest_api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import midterm.class_student_rest_api.model.Course;
import midterm.class_student_rest_api.model.Student;
import midterm.class_student_rest_api.repository.CourseRepository;
import midterm.class_student_rest_api.repository.StudentRepository;

@Service
public class StudentService {
    @Autowired
    StudentRepository studentRepository;

    @Autowired
    CourseRepository courseRepository;

    // Hàm tạo student mới:
    public Student createStudent(Long courseId, Student inputStudent) {
        Optional<Course> courseData = courseRepository.findById(courseId);
        Student result = new Student();
        if (courseData.isPresent()) {
            Course _course = courseData.get();
            Student newStudent = new Student();
            newStudent.setStudentName(inputStudent.getStudentName());
            newStudent.setGender(inputStudent.getGender());
            newStudent.setNgaySinh(inputStudent.getNgaySinh());
            newStudent.setAddress(inputStudent.getAddress());
            newStudent.setStudentPhone(inputStudent.getStudentPhone());
            newStudent.setCourse(_course);
            result = studentRepository.save(newStudent);
        }
        return result;
    }

    // Hàm lấy student list:
    public List<Student> getStudentList() {
        List<Student> optList = new ArrayList<>();
        studentRepository.findAll().forEach(optList::add);
        return optList;
    }

    // Hàm lấy học sinh bằng id:
    public Student getStudentById(Long studentId) {
        Optional<Student> studentData = studentRepository.findById(studentId);
        Student result = new Student();
        if (studentData.isPresent()) {
            result = studentData.get();
        } else {
            result = null;
        }
        return result;
    }

    // Hàm update học sinh:
    public Student updateStudent(Long studentId, Student inputStudent) {
        Optional<Student> studentData = studentRepository.findById(studentId);
        Student result = new Student();
        if (studentData.isPresent()) {
            Student updatingStudent = studentData.get();
            updatingStudent.setStudentName(inputStudent.getStudentName());
            updatingStudent.setGender(inputStudent.getGender());
            updatingStudent.setNgaySinh(inputStudent.getNgaySinh());
            updatingStudent.setAddress(inputStudent.getAddress());
            result = studentRepository.save(updatingStudent);
        } else {
            result = null;
        }
        return result;
    }

    // Hàm xóa học sinh:
    public void deleteStudent(Long studentId) {
        studentRepository.deleteById(studentId);
    }
}
