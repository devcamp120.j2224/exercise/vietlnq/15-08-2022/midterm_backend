package midterm.class_student_rest_api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import midterm.class_student_rest_api.model.Course;
import midterm.class_student_rest_api.model.Student;
import midterm.class_student_rest_api.repository.CourseRepository;
import midterm.class_student_rest_api.repository.StudentRepository;

@Service
public class CourseService {
    @Autowired
    CourseRepository courseRepository;

    @Autowired
    StudentRepository studentRepository;

    // Hàm tạo course mới: 
    public Course createCourse(Course inputCourse) {
        Course newCourse = new Course();
        newCourse.setCourseName(inputCourse.getCourseName());
        newCourse.setTeacherName(inputCourse.getTeacherName());
        newCourse.setTeacherPhone(inputCourse.getTeacherPhone());
        Course savedCourse = courseRepository.save(newCourse);
        return savedCourse;
    }

    // Hàm lấy course list:
    public List<Course> getCourseList() {
        List<Course> courseList = new ArrayList<>();
        courseRepository.findAll().forEach(courseList::add);
        return courseList;
    }

    // Hàm lấy detail 1 course bằng id:
    public Course getCourseById(long courseId) {
        Optional<Course> courseData = courseRepository.findById(courseId);
        Course result = new Course();
        if (courseData.isPresent()) {
            result = courseData.get();
        } else {
            result = null;
        }
        return result;
    }

    // Hàm update course:
    public Course updateCourse(long courseId, Course inputCourse) {
        Optional<Course> courseData = courseRepository.findById(courseId);
        Course opt = new Course();
        if (courseData.isPresent()) {
            Course updatingCourse = courseData.get();
            updatingCourse.setCourseName(inputCourse.getCourseName());
            updatingCourse.setTeacherName(inputCourse.getTeacherName());
            updatingCourse.setTeacherPhone(inputCourse.getTeacherPhone());
            opt = courseRepository.save(updatingCourse);
        }
        return opt;
    }

    // Hàm xóa course:
    public void deleteCourse(Long courseId) {
        // Xóa các student trong course:
        Optional<Course> courseData = courseRepository.findById(courseId);
        if (courseData.isPresent()) {
            Course course = courseData.get();
            Set<Student> students = course.getStudents();
            for (Student currStudent : students) {
                studentRepository.delete(currStudent);
            }
            courseRepository.deleteById(courseId);
        }
    }

}
