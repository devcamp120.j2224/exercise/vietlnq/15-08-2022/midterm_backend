package midterm.class_student_rest_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassStudentRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassStudentRestApiApplication.class, args);
	}

}
