package midterm.class_student_rest_api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import midterm.class_student_rest_api.model.Course;
import midterm.class_student_rest_api.service.CourseService;

@RestController
@RequestMapping("/")
public class CourseController {
    @Autowired
    CourseService courseService;

    // Tạo course mới
    @CrossOrigin
    @PostMapping("/course/create")
    public ResponseEntity<Course> createCourse(@Valid @RequestBody Course inputCourse) {
        try {
            return new ResponseEntity<>(courseService.createCourse(inputCourse), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } 
    }

    // Lấy course list
    @CrossOrigin
    @GetMapping("/course/all")
    public ResponseEntity<List<Course>> getCourseList() {
        try {
            return new ResponseEntity<>(courseService.getCourseList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } 
    }

    // Lấy course bằng id:
    @CrossOrigin
    @GetMapping("/course/detail/{courseId}")
    public ResponseEntity<Course> getCourseById(@PathVariable("courseId") long courseId) {
        try {
            Course _course = courseService.getCourseById(courseId);
            if(_course != null){
                return new ResponseEntity<Course>(_course, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Update Course:
    @CrossOrigin
    @PutMapping("/course/update/{courseId}")
    public ResponseEntity<Course> updateCourse(@PathVariable("courseId") long courseId, @RequestBody Course inputCourse) {
        try {
            return new ResponseEntity<>(courseService.updateCourse(courseId, inputCourse), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Course:
    @CrossOrigin
    @DeleteMapping("/course/delete/{courseId}")
    public ResponseEntity<Object> deleteCourse(@PathVariable("courseId") long courseId) {
        try {
            courseService.deleteCourse(courseId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
