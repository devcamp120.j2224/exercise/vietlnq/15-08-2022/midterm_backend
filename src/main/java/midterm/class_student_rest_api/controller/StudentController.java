package midterm.class_student_rest_api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import midterm.class_student_rest_api.model.Student;
import midterm.class_student_rest_api.service.StudentService;

@RestController
@RequestMapping("/")
public class StudentController {
    @Autowired
    StudentService studentService;

    // Tạo học sinh mới:
    @CrossOrigin
    @PostMapping("/student/create/{courseId}")
    public ResponseEntity<Student> createStudent(@PathVariable("courseId") Long courseId, @RequestBody Student inputStudent) {
        try {
            return new ResponseEntity<>(studentService.createStudent(courseId, inputStudent), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } 
    }

    // Lấy danh sách học sinh:
    @CrossOrigin
    @GetMapping("/student/all")
    public ResponseEntity<List<Student>> getStudentList() {
        try {
            return new ResponseEntity<>(studentService.getStudentList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } 
    }

    // Lấy chi tiết học sinh bằng id:
    @CrossOrigin
    @GetMapping("/student/detail/{studentId}")
    public ResponseEntity<Student> getStudentById(@PathVariable("studentId") Long studentId) {
        try {
            Student _student = studentService.getStudentById(studentId);
            if (_student != null) {
                return new ResponseEntity<>(studentService.getStudentById(studentId), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } 
    }

    // Update học sinh:
    @CrossOrigin
    @PutMapping("/student/update/{studentId}")
    public ResponseEntity<Student> updateStudent(@PathVariable("studentId") Long studentId, @RequestBody Student inputStudent) {
        try {
            Student _student = studentService.updateStudent(studentId, inputStudent);
            if (_student != null) {
                return new ResponseEntity<>(_student, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } 
    }

    // Xóa học sinh:
    @CrossOrigin
    @DeleteMapping("/student/delete/{studentId}")
    public ResponseEntity<Object> deleteStudentById(@PathVariable("studentId") Long studentId) {
        try {
            studentService.deleteStudent(studentId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
