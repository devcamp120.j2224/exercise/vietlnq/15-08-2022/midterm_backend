package midterm.class_student_rest_api.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "Nhap ten hoc sinh")
    @Size(min = 2, message = "Ten hoc sinh phai co it nhat 2 ky tu")
    @Column(name = "student_name")
    private String studentName;

    @NotEmpty(message = "Nhap gioi tinh")
    @Column(name = "gender")
    private String gender;

    @NotEmpty(message = "Nhap ngay sinh")
    @Column(name = "date_of_birth")
    private String ngaySinh;

    @NotEmpty(message = "Nhap dia chi")
    @Column(name = "address")
    private String address;

    @Column(name = "student_phone")
    private String studentPhone;

    @ManyToOne
    @JoinColumn(name = "course_id")
    @JsonBackReference
    private Course course;

    public Student() {
    }

    public Student(int id, String studentName, String gender, String ngaySinh, String address,
            String studentPhone, Course course) {
        this.id = id;
        this.studentName = studentName;
        this.gender = gender;
        this.ngaySinh = ngaySinh;
        this.address = address;
        this.studentPhone = studentPhone;
        this.course = course;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
}
