package midterm.class_student_rest_api.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "Nhap ten lop")
    @Size(min = 2, message = "Ten lop phai co it nhat 2 ky tu")
    @Column(name = "course_name")
    private String courseName;

    @NotEmpty(message = "Nhap ten giao vien")
    @Column(name = "teacher_name")
    private String teacherName;

    @Column(name = "teacher_phone")
    private String teacherPhone;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Student> students;

    public Course() {
    }

    public Course(int id, String courseName, String teacherName, String teacherPhone, Set<Student> students) {
        this.id = id;
        this.courseName = courseName;
        this.teacherName = teacherName;
        this.teacherPhone = teacherPhone;
        this.students = students;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherPhone() {
        return teacherPhone;
    }

    public void setTeacherPhone(String teacherPhone) {
        this.teacherPhone = teacherPhone;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

}
